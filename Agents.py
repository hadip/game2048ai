import game2048
import random
from random import shuffle

# This class implements different agents with different strategies
class Agent:
    
    def __init__(self, index = 0 ):
        self.index = index

class randomAgent(Agent):
    
    # This function gets a state and return a action
    def getAction(self, state):
        
        choices = state.getLegalActions( self.index )
        
        # the random human agent just picks randomly with uniform dist.
        if self.index == 0:
            return random.choice(choices)
        # the random computer agent picks 4 w.p. 0.1
        else:
            if ( random.random() > 0.9 ): 
                return random.choice( choices[len(choices)/2:] )
            else: 
                return random.choice( choices[:len(choices)/2] )

class reflexAgent(Agent):
    
    def getAction(self, state):
        
        choices = state.getLegalActions( self.index )

        if self.index == 0:
            newStates = [ state.generateSuccessor(self.index, act) for act in choices ]
            newScores = [ s.score for s in newStates ]
            bestIndex = newScores.index( max( newScores ) )
            return choices[ bestIndex ]
        
        if self.index == 1:
            newStates = [ state.generateSuccessor(self.index, act) for act in choices ]
            newScores = [ s.score for s in newStates ]
            bestIndex = newScores.index( min( newScores ) )
            return choices[ bestIndex ]
        
            
class biasedAgent(Agent):
    
    def getAction(self, state):

        choices = state.getLegalActions( self.index )
        return choices[0]

class expectiMaxAgent(Agent):

    def __init__(self, index = 0, depth = 4):
        self.index = index
        self.depth = depth
        self.weights = { 2:0.9, 4:0.1 }
        self.epsilon = 0 # probability of risk

    def mocHumanEval(self,state):
        
        remainedToSee = (state.n)**2 -1 

        result = state.board[0][0] << 2* ( remainedToSee )
        remainedToSee -= 1
        lastSeen = state.board[0][0]

        for i in range(state.n):
            for j in range(state.n)[::(-1)**i]:
                if i == 0 and j == 0: continue
                elif state.board[i][j] <= lastSeen:
                    result += state.board[i][j] << 2 * ( remainedToSee )
                    remainedToSee -= 1
                    lastSeen = state.board[i][j]
                else:
                    lastSeen = -1
                    result += state.board[i][j] << remainedToSee
                    remainedToSee -= 1
                
	return result
	

    def evaluationFunction(self, state):
    #    return state.score
	return self.mocHumanEval(state)

    def getAction(self, state):
        
        n = 1 # the maximum index of an agent
        def recursiveVopt(s,p,d):
            # get legal actions for this agent
            legalMoves = s.getLegalActions(p)

	    if ( d == self.depth-1 ): 
                maxNumMoves = 4
            elif ( d == self.depth-2 ):
                maxNumMoves = 2
            else:
                maxNumMoves = 1

            if p == 1: 
                if len(legalMoves) > maxNumMoves:
                    if random.random()>self.epsilon:
                        legalMoves = legalMoves[:maxNumMoves]
                    else:
                        legalMoves = random.sample( legalMoves, maxNumMoves )
            
            
            # check if we are given an endState
            if s.isGameOver(): return (s.score,None)
            if len(legalMoves) == 0: return (s.score,None)

            # check if we are in the lowest level of the search subtree
            if d == 0: return (self.evaluationFunction(s),None)

            # define the recursion step of the next agent
            if p == 0: newd = d-1 
            else: newd = d

            # define the new agent
            newP = (p+1)%(n+1)

            # recurse when agent is human: (maximization)
            if p == 0:
                return max( [ (recursiveVopt(s.generateSuccessor(p,a),newP,newd)[0],a) for a in legalMoves] )
            else: # here is the stochastic part
                return  ( sum( [ self.weights[a[1]] * recursiveVopt(s.generateSuccessor(p,a),newP,newd)[0] for a in legalMoves] ) / sum( [ self.weights[a[1]] for a in legalMoves] ) , None)

        # Adaptive depth idea:
        '''
        If there are a few empty cells on the board, deepen more!
        '''
        numEmptyTiles = len( state.getLegalActions(1) ) / 2

        newDepth = self.depth
        # if numEmptyTiles <= 0: newDepth += 1

        Vopt , act = recursiveVopt(state, self.index, newDepth) 
        return act
