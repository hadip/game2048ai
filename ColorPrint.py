import sys
from termcolor import colored, cprint
import math
import game2048

class ColorPrint:
    def __init__(self):
        self.colorMap = ['red', 'green', 'yellow', 'magenta','blue' ,'cyan', 'grey','white']
        self.backColor = [ 'on_'+c for c in self.colorMap ]
        self.attributes = ['bold','dark','underline','blink','reverse','concealed']
        self.stringLength = 7 # length of string of each number 
        self.halfTileHeight = 1 # half of the height of the tile 

    # Gets an integer number and outputs a string s.t., the given number is in mid
    def getNumString(self, m ):
        if m == 0 : return ' '*self.stringLength
        k = float(len(str(m)))
        # number of spaces at left and right:
        numSpacesLeft = int( math.ceil ( ( self.stringLength - k ) / 2. ) )
        numSpacesRight = int( math.floor ( ( self.stringLength - k ) / 2. ) )
        s =  ' '*numSpacesLeft + str(m) + ' '*numSpacesRight

        # Now, color it !
        index =  int(math.log(m)/math.log(2)) - 1
        color = self.colorMap[ index/2 ]
        if index % 2 != 0:
            attrsList = []
        else:
            attrsList = self.attributes[:2]
        return colored(s, color , attrs = attrsList )
                
    def printColorBoard(self, state ):
        text = (' '+'-'*self.stringLength)*state.n + ' \n'
        for i in range(state.n):
            # space row
            for j in range(self.halfTileHeight):
                text += ( ('|'+' '*self.stringLength)*state.n + '|\n')
            # number row
            rowString = '|'
            for j in range(state.n):
                rowString += ( self.getNumString(state.board[i][j])+'|')
            text += (rowString+'\n')
            # space row
            for j in range(self.halfTileHeight):
                text += ( ('|'+' '*self.stringLength)*state.n + '|\n')
            text += (' '+'-'*self.stringLength)*state.n + ' \n'
        return text
