import game2048
import Agents
import ColorPrint
import time
import copy

class Game:
    def __init__( self, state, agents, dispDelayTimeHuman = .01, dispDelayTimeComputer = .01):
        self.state = state
        self.human = agents[0]
        self.computer = agents[1]
        self.dispDelayTimeHuman = dispDelayTimeHuman # in seconds
        self.dispDelayTimeComputer = dispDelayTimeComputer # in seconds
        self.colorPrinter = ColorPrint.ColorPrint()
        self.numMoves = 0

    # Initialize the game with two tiles on the board
    def initialize(self):
        self.state = copy.deepcopy( self.state.generateSuccessor( self.computer.index, self.computer.getAction(self.state) ) )
        self.state = copy.deepcopy( self.state.generateSuccessor( self.computer.index, self.computer.getAction(self.state) ) )

    def play(self):
        startTime = time.time()
        while not self.state.isGameOver():
            self.state = copy.deepcopy( self.state.generateSuccessor( self.human.index, self.human.getAction(self.state) ) )
            self.state.printBoard(self.colorPrinter)
            print 'Number of moves = '+str(self.numMoves)
            time.sleep(self.dispDelayTimeHuman)
            self.state = copy.deepcopy( self.state.generateSuccessor( self.computer.index, self.computer.getAction(self.state) ) )
            self.state.printBoard(self.colorPrinter)
            print 'Number of moves = '+str(self.numMoves)
            time.sleep(self.dispDelayTimeComputer)
            self.numMoves += 1
            
        elapsedTime = time.time() - startTime

        maxOnBoard = max( [ self.state.board[i][j] for i in range(self.state.n) for j in range(self.state.n) ] )
        
        return (self.state.score, maxOnBoard, self.numMoves, elapsedTime)
            
if __name__ == '__main__':
    '''
    Output file format:
    Each line has the following information for each simulation
    Score, Maximum tile no board, number of moves, time of the game
'''
    numRun = 1
    outFile = open('results.txt','w')
    for i in range(numRun):
        game  = Game( game2048.State(4), [ Agents.expectiMaxAgent(0), Agents.randomAgent(1)] )
        game.initialize()
        stat = game.play()
        outFile.write("%d %d %d %f\n"%stat)
        print "Game "+str(i)+" Done!"
    print "*"*20
