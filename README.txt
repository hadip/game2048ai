\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
	AI ALGORITHM FOR THE GAME 2048
	 Hadi Pouransari, Saman Ghili
	 Contact: hadip@stanford.edu
	 	  samang@stanford.edu
	      December 12 2014
/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\

This is a python simulator/solver for the game 2048.
You can find the original version of the game at:
2048game.com/

This code includes following files:

------------------
game2048.py
------------------
In this file, we have defined the class "state" with its corresponding functions.
All the game rules are incorporated in this class.
The most useful functions (in terms of their application in the AI algorithms) are:
1) generateSuccesor( agent index, action): This generate new game state based on the action.
2) isGameOver(): Tells you if you are in an end state.
3) getLegalActions( agent index): returns a list of legal action for the given agent.
Note that a state can be any (n x n) board. You may pass n to constructor (default = 4) when creating a state object.

------------------
Agents.py
------------------
In this file we have implemented various agents:
1) randomAgent
2) reflexAgent
3) biasedAgent
4) expectiMaxAgent
Each agent should has a function getAction( state ), which gets a state and return the action.

------------------
ColorPrint.py
------------------
The class ColorPrint takes care of printing the board (in color) to consule.
We ar eusing the "termcolor" package here. The file termcolor.py is added also.

------------------
Game.py
------------------
Here we have implemented the general calss Game. This class takes care of running the game.
In order to create a game you need to pass couple of things to Game. For instance:
game = Game( game2048.State(n = 4), [ Agents.reflexAgent(0), Agents.randomAgent(1)], dispDelayTimeHuman = .5, dispDelayTimeComputer = .5)
It means we will have a 4 by 4 board of the game 2048, with two agents reflex (for human), and random (for computer). The last two parameters are the times (in seconds) to display board after each move.

\*/*\*\*/*\*\*/*\*\*/*\*\*/*\*
             RUN
\*/*\*\*/*\*\*/*\*\*/*\*\*/*\*

To run the code just type in command line (in the directory of project):
python Game.py

If you want to change size of the board, or agents, you need to change it in Game.py (at the end of the file where game is defined).
Currently it is set to:
Human agent: pruned expectimax with depth 3, and S-shaped evaluation function
Computer agent: random

\*/*\*\*/*\*\*/*\*\*/*\*\*/*\*
             DATA
\*/*\*\*/*\*\*/*\*\*/*\*\*/*\*

You can run the code from the folder Code. In this case you will see the visualization, and it will be runned for one time.
We have collected data usnig differnet agents for many simulations.
The resulted data (and the code) for each agent is in the separte folder.
Also, if you want to change the number of simulation, you can change variable numRun in Game.py.
The results of the simulation will be stored in a file results.txt in the same directory.
Each line of the file result.txt contains the following information for one simulation:
Score, Maximum tile value on board, Number of Moves, Time of the game (in seconds)
