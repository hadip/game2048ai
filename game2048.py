import copy
import os
import Agents
import sys
import ColorPrint

class State:
    # This class store a state of the game in a nxn tuple of tuple
    def __init__(self, n = 4):
        self.n = n
        self.board = []
        for i in range(n):
            self.board.append([0 for j in range(n)])
        self.score = 0
    
    # Copy current state state to a new state
    def stateDeepCopy(self):
        newState = State( self.n )
        newState.board = copy.deepcopy(self.board)
        newState.score = self.score
        return newState

    # Check if the given state board is equal to the current board
    def isEqual(self, state):
        for i in range(self.n):
            for j in range(self.n):
                if self.board[i][j] != state.board[i][j]: return False
        return True

    # Get possible (not necessarily legal) actions for agent = 0
    def getPossibleActions(self):
        return ['left', 'up', 'right', 'down']

    # Check if any action is possible
    def isGameOver(self):
        """
        Note that based on the rule of the game, it is sufficient to check if the "player" agenet 
        has any action.
        """
        return len( self.getLegalActions() ) == 0

    
    '''
    This function takes a [direciton], and rotates the board such that applying that direction to \
    the original board is equivalent to applying pushLeft to the rotated board.
    '''
    def rotate(self, direction):
        
        # the [direction]='down' is the default
        if direction == 'left': return

        
        if direction == 'right':
            for i in range(self.n):
                self.board[i].reverse()
            return
        
        
        # Create a copy of the current board (we need it for action = down, up)
        copyBoard = copy.deepcopy(self.board)

        # result is action = 'down'
        """
        1   2   3   4             13  9   5   1
        5   6   7   8     ===>    14  10  6   2
        9   10  11  12    ===>    15  11  7   3
        13  14  15  16            16  12  8   4
        """
        if direction == 'down':
            for i in range(self.n):
                col = self.n - i - 1
                for j in range(self.n):
                    self.board[j][col] = copyBoard[i][j]
            return        

        # result is action = 'up'
        """
        1   2   3   4             4   8  12  16
        5   6   7   8     ===>    3   7  11  15
        9   10  11  12    ===>    2   6  10  14
        13  14  15  16            1   5  9   13  
        """
        if direction == 'up':
            for i in range(self.n):
                for j in range(self.n):
                    row = self.n - j - 1
                    self.board[row][i] = copyBoard[i][j]
    
    # This function rotates the board back to its original orientation (after rotate() is called)
    def rotateBack(self, direction):
        if direction == 'left': return
        if direction == 'right':
            self.rotate('right')
            return
        if direction == 'down':
            self.rotate('up')
            return
        if direction == 'up':
            self.rotate('down')
            return

    # This function gets a column, and updates it assuming action = "left"
    """
    It returns the reward.
    Note that the value of input row will be changed (it is pass by reference!)
    """
    def pushLeftRow(self, row):
        # First push everything to the left
        lastLoc = 0
        for i in range(len(row)):
            if row[i] != 0:
                if i != lastLoc:
                    row[lastLoc] = row[i]
                    row[i] = 0
                lastLoc += 1
        # Now merge same neighbors:
        reward = 0
        for i in range(len(row)-1):
            if row[i] == row[i+1]:
                row[i] *= 2
                reward += row[i]
                # shift everything to the left
                for j in range(i+2,len(row)):
                    row[j-1] = row[j]
                # the last element should be zero now
                row[-1] = 0
        return reward

    # This function apply pushLeftRow to every row
    def pushLeft(self):
        totReward = 0
        for i in range(self.n):
            totReward += self.pushLeftRow(self.board[i])
        return totReward

    # Given an action and agent compute the next state
    """
    If the action is not legal it returns None.
    """
    def generateSuccessor(self, agentIndex, action):

        # Player agent
        if agentIndex == 0:

            # Copy the current state
            newState = self.stateDeepCopy()

            # First rotate the newState based on the given action
            newState.rotate( action )

            # Push the rotated board to left
            reward = newState.pushLeft()

            # Rotate back
            newState.rotateBack( action )
            
            # Now check if the given |action| has actually done somthing!
            if self.isEqual( newState ): return None
            else:
                newState.score += reward
                return newState

        # Computer agent
        elif agentIndex == 1:
            (i,j),v = action
            
            # Check if the action is possible
            if self.board[i][j] != 0: return None
            
            # Copy the current state
            newState = self.stateDeepCopy()

            # Add the new element
            newState.board[i][j] = v
            
            return newState

    # Returns the legal actions of each agent. 0:player, 1:computer
    def getLegalActions( self, agentIndex = 0 ):
        """
        For the player-agent:
        The possible actions are: ['left','right','up','down']

        For the computer agent. 
        The actions are tupe( (i,j) , v)
        This means put a tile with value v at position (i,j) 
        """

        # Player agent
        if agentIndex == 0:
            actions = []
            for act in self.getPossibleActions():
                if self.generateSuccessor( agentIndex, act ) != None:
                    actions.append( act )
            return actions
        
        # Computer agent
        elif agentIndex == 1:
            # find the empty cells
            emptyCells = []
            for i in range(0,self.n):
                for j in range(0,self.n)[::(-1)**i]:
                    if self.board[i][j] == 0:                        
                        emptyCells.append( (i,j) )
            
            # the possible new values
            newValues = [2,4]

            # if the board is full there is no move for the computer
            if len(emptyCells) == 0: return []
            
            return [ (cell,v) for v in newValues for cell in emptyCells ]    

    def printBoard(self, colorPrinter):
        os.system('clear')
        print colorPrinter.printColorBoard(self)
        print 'Score= ' + str(self.score)
